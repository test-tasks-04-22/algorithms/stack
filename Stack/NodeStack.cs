﻿using System.Collections;
using System.Diagnostics;

namespace Stack;

public class Node<T>
{
    public Node(T data)
    {
        Data = data;
    }
    public T Data { get; }
    public Node<T>? Next { get; init; }
}

public class NodeStack<T> : IEnumerable<T>
{
    private Node<T>? _head;

    private bool IsEmpty => Count == 0;

    private int Count { get; set; }

    public void Push(T item)
    {
        var node = new Node<T>(item)
        {
            Next = _head
        };
        _head = node;
        Count++;
    }
    public T Pop()
    {
        if (IsEmpty)
            throw new InvalidOperationException("Стек пуст");
        var temp = _head;
        _head = _head?.Next;
        Count--;
        return temp!.Data;
    }
    public T Peek()
    {
        if (IsEmpty)
            throw new InvalidOperationException("Стек пуст");
        Debug.Assert(_head != null, nameof(_head) + " != null");
        return _head.Data;
    }
 
    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable)this).GetEnumerator();
    }
 
    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
        var current = _head;
        while (current != null)
        {
            yield return current.Data;
            current = current.Next;
        }
    }
}