﻿using Stack;

var stack = new NodeStack<string>();
stack.Push("1");
stack.Push("2");
stack.Push("3");
stack.Push("4");
 
Console.Write("Стек после добавления элементов: ");
foreach (var item in stack)
{
    Console.Write($"{item} ");
}

string header = stack.Peek();
Console.WriteLine($"\nПоследний элемент стека: {header}");

stack.Pop();
Console.Write("Стек после удаления последнего элемента: ");
foreach (var item in stack)
{
    Console.Write($"{item} ");
}
